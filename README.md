# AF2ConfDiv-Oct2021

This repository includes R scripts and data files related with our upcoming manuscript *Impact of protein conformational diversity on AlphaFold predictions*. 

In the **src** folder there is an RMarkdown file **paper-figures-26jan-rev.Rmd** which can be run to reproduce all figures in the manuscript from the attached data in the **data** folder. The file **data/data_description.md** describes the contents of the data files.

The HTML file **/src/paper-figures-26jan-rev.html** allows for easier visualization of the code and figures mentioned above.

