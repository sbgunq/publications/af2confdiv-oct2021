# From /26-01-2022 folder
## revision1_86_plus_5.csv
- **apo_id**: PDB_ID code of Apo conformation.
- **holo_id**: PDB_ID code of Holo conformation.
- **rmsd_apo_holo**: RMSD between apo and holo conformation.
- **RMSD_best_score_md_Apo**: RMSD between the model of best pLLDT score and Apo conformation.
- **RMSD_best_score_md_Holo**: RMSD between the model of best pLLDT score and Apo conformation.
- **mds_best_global_score**: models with the highest pLLDT average score.
- **length**: length of each protein.
- **seqs_sligned**: number of sequences of MSA.
- **lowest_rmsd_md_apo_or_holo**: RMSD between Apo or Holo structure and the model with the lowest RMSD against Apo and Holo forms.
- **type**: if the lowest RMSD is obtained against Apo or Holo form.
- **lowest_md_score**: plDDT score of the model with the lowest RMSD.
- **doamins_quant**: quantity of protein domains.
- **motion_type**: type of motions between domains using DynDom software v1.5 (Taylor et al., 2013) (domain movements: two or more domains presenting hinge movements; loop movements: one domain, movements due to loops).

## lowest_rmsd_window_15_86proteins_plus_5.csv 
- **scores**: pIDDT score of the lowest model (RMSD based) for a protein.
- **rmsf**: RMSF between alpha carbons of model with the lowest RMSD.
- **Score_Lower_rmsd_M_factor**: factor column of score.

## tabla_Score_Global_86_plus_5.csv
- **Model_ID**: ID of AlphaFold model.
- **type_score**: global plDDT score.
- **score**: value of plDDT.

## Table_degree_prom_86_plus_5.csv
- **Apo_ID**: PDB_ID code of Apo conformation.
- **Holo_ID**: PDB_ID code of Holo conformation.
- **Apo_degree_prom**: average number of inter-residue contacts of Apo conformation.
- **Holo_degree_prom**: average number of inter-residue contacts of Holo conformation.

## Table_lowest_compare_vs_Apo_vs_Holo_95_plus_5.csv
- **Apo_ID**: PDB_ID code of Apo conformation.
- **Holo_ID**: PDB_ID code of Holo conformation.
- **Model_ID**: ID of AlphaFold model.
- **Model_ID(lowest_rmsd_vs_Apo)**: ID of AlphaFold model with the lowest RMSD against Apo conformation.
- **Model_ID(lowest_rmsd_vs_Holo)**: ID of AlphaFold model with the lowest RMSD against Holo conformation.
- **RMSD_vs_Apo**: RMSD between Apo conformation and Model_ID(lowest_rmsd_vs_Apo).
- **RMSD_vs_Holo**: RMSD between Holo conformation and Model_ID(lowest_rmsd_vs_Holo).

## Table_rmsd_vs_model-OK_95_plus_5.csv
- **Apo_ID**: PDB_ID code of Apo conformation.
- **Holo_ID**: PDB_ID code of Holo conformation.
- **Model_ID**: ID of AlphaFold model.
- **RMSD_vs_Apo**: RMSD between Model_ID and Apo conformation.
- **RMSD_vs_Holo**: RMSD between Model_ID and Holo conformation.

## Table_prom_rmsd_vs_Apo_prom_rmsd_vs_Holo_95_plus_5.csv
- **Type**: if the lowest RMSD is obtained against Apo or Holo form.
- **Apo_ID**: PDB_ID code of Apo conformation.
- **Holo_ID**: PDB_ID code of Holo conformation.
- **Model_ID_lowest_RMSD_vs_Apo/Holo**: ID of AlphaFold model with the lowest RMSD against Apo or Holo conformation.
- **lowest_RMSD_vs_Apo_Holo**: RMSD between Apo or Holo conformation and the model displaying the lowest RMSD.
- **RMSD_Apo_vs_Holo**: RMSD between apo and holo conformation.
- **RMSD_prom_models_vs_Apo**: average RMSD between Apo conformation and all the AlfaFold models obtained.
- **RMSD_prom_models_vs_Holo**: average RMSD between Holo conformation and all the AlfaFold models obtained.

## Table_Rg_data_set_5_2022.csv / Table_Rg_data_set_86.csv
- **Apo_ID**: PDB_ID code of Apo conformation.
- **Holo_ID**: PDB_ID code of Holo conformation.
- **RG_Apo**: Radius of gyration of Apo conformation.
- **RG_Holo**: Radius of gyration of Holo conformation

## Table_score_lower_rmsd_model_vs_rmsf_ApoHolo_25_01_2022 / Table_score_lower_rmsd_model_vs_rmsf_ApoHolo_86_plus_5.csv
- **Apo_ID**: PDB_ID code of Apo conformation.
- **Holo_ID**: PDB_ID code of Holo conformation.
- **Model_ID**: ID of AlphaFold model.
- **Score_Lower_rmsd_M**: pIDDT score of the lowest model (RMSD based) for a protein.
- **RMSF_CA**: RMSF between alpha carbons of model with the lowest RMSD.

## clusters_maxRMSD_stats(Xray).csv
- **cluster**: CoDNaS cluster ID.
- **count**: number of conformers present in the cluster.
- **mean**: mean RMSD of the different conformations in the cluster.
- **std**: standard deviation of RMSD of the different conformations in the cluster.
- **min**: minimum RMSD of the different conformations in the cluster.
- **25%**: 25-percentile RMSD of the different conformations in the cluster.
- **median**: median RMSD of the different conformations in the cluster.
- **75%**: 75-percentile RMSD of the different conformations in the cluster.
- **max**: max RMSD of the different conformations in the cluster.

## clusters.csv
- **cluster**: relative ID for the study.
- **pdb**: CoDNaS cluster ID.
- **minRMSD**: minimum RMSD of the different conformations in the cluster.
- **maxRMSD**: maximum RMSD of the different conformations in the cluster.
- **Average RMSD**: average RMSD for that cluster.
- **Sigma**: standard deviation of RMSD of the different conformations in the cluster.
- **Dispersa (Sigma>2)**: proteins/clusters with a σ greater than 2 are classified as Disperse.
- **Rigida (RMSD<1.0)**: proteins/clusters with a σ lower than 1 are annotated as Rigid.
- **RMSDlower**: lowest RMSD measured between any AF2 model and the representative PDB of the cluster (pdb column).

## 196_lowestRmsd_with_method_maxs.csv
- **Cluster_ID**: cluster number.
- **codnas3_ID**: PDB representative of CoDNaS cluster.
- **Model_ID**: ID of AlphaFold model.
- **RMSD_mammoth**: between best model and representative PDB.
- **Number_of_res_align**: according to the alignment.
- **max_RMSD**: maximum RMSD of CoDNaS cluster.
- **methods**: experimental methods which were used to obtain the structure protein of the maximum RMSD pair.
- **pair**: PDB codes for the proteins of the maximum RMSD pair.

